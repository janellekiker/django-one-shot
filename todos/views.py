from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoListUpdateForm, TodoItemForm, TodoItemUpdateForm


# Create a view to get all the instances of TodoList model
# and put them in the context for the template
def todo_list_list(request):
    all_todos = TodoList.objects.all()
    context = {
        "todo_list_list": all_todos,
    }
    return render(request, "todos/todo_list_list.html", context)


# Lists all the tasks in the todo_list
def todo_list_detail(request, id):
    task = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_tasks": task,
    }
    return render(request, "todos/todo_list_detail.html", context)


# Create a new todo list
def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }

    return render(request, "todos/todo_list_create.html", context)


# Update todo list name
def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListUpdateForm(request.POST, instance=todo_list)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {
        "todo_list_list": todo_list,
        "form": form,
    }

    return render(request, "todos/todo_list_update.html", context)


# Delete a Todo list
def todo_list_delete(request, id):
    list_to_delete = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list_to_delete.delete()
        return redirect("todo_list_list")
    return render(request, "todos/todo_list_delete.html")


# Create a todo list ITEM
def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            # redirect to the detail page for that to-do list
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }

    return render(request, "todos/todo_item_create.html", context)


# Update todo item
def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemUpdateForm(request.POST, instance=todo_item)
        if form.is_valid():
            todo_item = form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)

    context = {
        "todo_list_item": todo_item,
        "form": form,
    }

    return render(request, "todos/todo_item_update.html", context)
